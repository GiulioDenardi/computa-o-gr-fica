#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui -> setupUi ( this );
    connect(ui->btnSum ,SIGNAL(clicked ()),this ,SLOT(sumNumbers()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow :: sumNumbers ( void )
{
    int r = ui->lineN1->text().toInt() + ui->lineN2->text().toInt();
    ui ->labelResult->setText(QString(" Result : %1").arg(r));
}
