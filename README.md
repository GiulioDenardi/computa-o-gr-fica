# README #

This project was created for storing activities for the "Computer Graphics" discipline from my university (UFABC).

Each folder represents an activity and furthermore, for the final projects, I'll create a brand new particular repository.

For now, there are these projects:

See [Summer activity](#markdown-header-summer)

See [Vectorizing activity](#markdown-header-vectorizing)


## Activities ##

### Summer

No, that's not a summer project.

It's the discipline's first exercise, the simplest project:

It takes two integers and sums into a LineText.

![SummerSS.jpg](https://bitbucket.org/repo/yajrnq/images/2443899138-SummerSS.jpg)

### Vectorizing

Vectorizing is a project that takes two Vectors and allows the user to do the following functions:

* Get the vectors' dot product
* Get the vectors' smallest angle
* Get the vectors' orthogonal vector
* Get the vectors' triangle area

![VectorizingSS.jpg](https://bitbucket.org/repo/yajrnq/images/1952417249-VectorizingSS.jpg)

Furthermore, there will be more projects as I advance in the discipline.