#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots :
    void dotProduct (void);
    void smallerAngle (void);
    void orthogonalVector (void);
    void triangleArea (void);


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
