#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "math.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->dotProdButton, SIGNAL(clicked()), this, SLOT(dotProduct()));
    connect(ui->smallAngButton, SIGNAL(clicked()), this, SLOT(smallerAngle()));
    connect(ui->orthogVectButton, SIGNAL(clicked()), this, SLOT(orthogonalVector()));
    connect(ui->triAreaButton, SIGNAL(clicked()), this, SLOT(triangleArea()));

    connect(ui->quitButton, SIGNAL(clicked()), this, SLOT(close()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow :: dotProduct ( void ) {
    //Pega vetores.
    float v1x = ui->v1x->text().toFloat(), v1y = ui->v1y->text().toFloat(), v1z = ui->v1z->text().toFloat();
    float v2x = ui->v2x->text().toFloat(), v2y = ui->v2y->text().toFloat(), v2z = ui->v2z->text().toFloat();

    float v1[] = {v1x, v1y, v1z};
    float v2[] = {v2x, v2y, v2z};

    float dotProduct = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];

    ui->dotProdResult->setText(QString("%1").arg(dotProduct));
}

void MainWindow :: smallerAngle ( void ) {
    //Pega vetores.
    float v1x = ui->v1x->text().toFloat(), v1y = ui->v1y->text().toFloat(), v1z = ui->v1z->text().toFloat();
    float v2x = ui->v2x->text().toFloat(), v2y = ui->v2y->text().toFloat(), v2z = ui->v2z->text().toFloat();

    float v1[] = {v1x, v1y, v1z};
    float v2[] = {v2x, v2y, v2z};

    float dotProduct = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];

    float vectorAbs1 = sqrt(pow(v1[0],2) + pow(v1[1],2) + pow(v1[2],2));
    float vectorAbs2 = sqrt(pow(v2[0],2) + pow(v2[1],2) + pow(v2[2],2));

    double smallAng = acos(dotProduct/(vectorAbs1*vectorAbs2));

    ui->smallAngResult->setText(QString("%1").arg(smallAng, 3, 'f', 3, '0'));
}

void MainWindow :: orthogonalVector ( void ) {
    //Pega vetores.
    float v1x = ui->v1x->text().toFloat(), v1y = ui->v1y->text().toFloat(), v1z = ui->v1z->text().toFloat();
    float v2x = ui->v2x->text().toFloat(), v2y = ui->v2y->text().toFloat(), v2z = ui->v2z->text().toFloat();

    float v1[] = {v1x, v1y, v1z};
    float v2[] = {v2x, v2y, v2z};

    float orthogVectorX = v1[1]*v2[2] - v1[2]*v2[1];
    float orthogVectorY = v1[2]*v2[0] - v1[0]*v2[2];
    float orthogVectorZ = v1[0]*v2[1] - v1[1]*v2[0];

    ui->orthogVectorX->setText(QString("%1").arg(orthogVectorX));
    ui->orthogVectorY->setText(QString("%1").arg(orthogVectorY));
    ui->orthogVectorZ->setText(QString("%1").arg(orthogVectorZ));
}

void MainWindow :: triangleArea ( void ) {
    //Pega vetores.
    float v1x = ui->v1x->text().toFloat(), v1y = ui->v1y->text().toFloat(), v1z = ui->v1z->text().toFloat();
    float v2x = ui->v2x->text().toFloat(), v2y = ui->v2y->text().toFloat(), v2z = ui->v2z->text().toFloat();

    float v1[] = {v1x, v1y, v1z};
    float v2[] = {v2x, v2y, v2z};

    float orthogVector[] = {v1[1]*v2[2] - v1[2]*v2[1],
                            v1[2]*v2[0] - v1[0]*v2[2],
                            v1[0]*v2[1] - v1[1]*v2[0]};

    float triArea = 0.5*sqrt(pow(orthogVector[0],2) + pow(orthogVector[1],2) + pow(orthogVector[2],2));

    ui->triAreaResult->setText(QString("%1").arg(triArea));
}
